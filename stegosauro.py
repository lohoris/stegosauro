#!/usr/bin/env python3

"""
	stegosauro (c) 2002-2012 Lorenzo "Lohoris" Petrone
	<looris@gmail.com> http://looris.net
	
	Copying and distribution of this file, with or without modification,
	are permitted in any medium without royalty provided the copyright
	notice and this notice are preserved.  This file is offered as-is,
	without any warranty.
"""

import sys
import os
import stat
import random

def die (message, errorlevel):
	print('stegosauro: '+message)
	sys.exit(errorlevel)

def sec (f_stat):
	return (int(f_stat.st_atime) & 0xFFFF)


command=''
target=''
message=''
do=None

###### parses command line options and checks them for errors

try:
	command=sys.argv[1]
	target=sys.argv[2]
except IndexError:
	die('syntax error',1)

try:
	target_stat=os.stat(target)
	if (not stat.S_ISDIR(target_stat.st_mode)):
		die("target isn't a directory ["+target+"]",3)
except OSError:
	die("target not found ["+target+"]",4)

if (command=='read' or command=='r'):
	do='read'
elif (command=='readhex' or command=='rh'):
	do='readhex'
elif	(command=='write' or command=='w'):
	# TODO: writehex, because it is much more useful to hide binary data than ascii, for obvious reasons
	do='write'
	try:
		message=sys.argv[3]
	except IndexError:
		die('syntax error',1)
elif	(command=='delete' or command=='del' or command=='d'):
	do='delete'
else:
	die('invalid command ['+command+']',2)

###### executes requested command

secs=bytearray()

if (do=='read' or do=='readhex'):
	#### gets the bytes
	
	xor=sec(target_stat)
	
	for path, dirs, files in os.walk(target):
		for file in files:
			fpath=os.path.join(path,file)
			sta=os.stat(fpath)
			sc=sec(sta) ^ xor
			secs.append(sc>>8)
			secs.append(sc&0xFF)
	
	#### prints them
	
	for by in secs:
		if (do=='readhex'):
			sys.stdout.write('%02x'%by)
		else:
			sys.stdout.write('%c'%by)
	
	#### re-adjusts the xor
	
	os.utime(target,(target_stat.st_atime,target_stat.st_mtime))
	
elif (do=='write'):
	#### creates the bytearray
	for ch in message:
		secs.append(ord(ch))
	
	xor = random.randint(0,0xFFFF)
	
	#### writes it to files
	for path, dirs, files in os.walk(target):
		# TODO: xor every directory, instead of just the target
		for file in files:
			fpath=os.path.join(path,file)
			sta=os.stat(fpath)
			sc=sec(sta)
			
			# currently it hardcodes 2 bytes per file
			ch0=0
			ch1=0
			try:
				ch0=secs.pop(0)
				ch1=secs.pop(0)
			except OverflowError:
				pass
			sc = ((ch0<<8) | ch1) ^ xor
			sys.stdout.write('%04x'%(sc))
			
			newa = (int(sta.st_atime) & ~(0xFFFF)) | sc
			os.utime(fpath,(newa,sta.st_mtime))
	
	newa = (int(target_stat.st_atime) & ~(0xFFFF)) | xor
	os.utime(target,(newa,target_stat.st_mtime))

elif (do=='delete')	:
	#### sets all the atimes equal to the mtimes, effectively deleting the message
	os.utime(target,(target_stat.st_mtime,target_stat.st_mtime))
	for path, dirs, files in os.walk(target):
		for file in files:
			fpath=os.path.join(path,file)
			sta=os.stat(fpath)
			os.utime(fpath,(sta.st_mtime,sta.st_mtime))


sys.stdout.write('\n')
